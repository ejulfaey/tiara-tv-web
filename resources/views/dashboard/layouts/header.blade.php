<nav class="navbar header-navbar pcoded-header">
    <div class="navbar-wrapper">
        <div class="navbar-logo">
            <a href="{{ route('dashboard') }}">
                <!-- <img class="img-fluid" src="{{ asset('themes/admindek/assets/icons/logo.png') }}" alt="Theme-Logo" /> -->
                Tiara_TV
            </a>
            <a class="mobile-menu" id="mobile-collapse" href="#!">
                <i class="fa fa-menu icon-toggle-right"></i>
            </a>
            <a class="mobile-options waves-effect waves-light">
                <i class="fa fa-more-horizontal"></i>
            </a>
        </div>
        <div class="navbar-container container-fluid">
            <ul class="nav-right">
                <li class="user-profile header-notification">
                    <div class="dropdown-primary dropdown">
                        <div class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ auth()->user()->image }}" class="img-radius" alt="User-Profile-Image">
                            <span>{{ auth()->user()->fullname }}</span>
                            <i class="fa fa-chevron-down"></i>
                        </div>
                        <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn"
                            data-dropdown-out="fadeOut">
                            <li>
                                <a href="#">
                                    <i class="fa fa-wrench"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-user-circle"></i> Profile
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}">
                                    <i class="fa fa-sign-out-alt"></i> Sign Out
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
