    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand:500,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito">
    <link rel="stylesheet" href="{{ asset('themes/admindek/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/admindek/css/waves.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/admindek/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/admindek/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/admindek/css/widget.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css?id=' . str_random(5)) }}">
