<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="pcoded-navigation-label">Main</div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="{{ active('dashboard') }}">
                    <a href="{{ route('dashboard') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fa fa-home"></i>
                        </span>
                        <span class="pcoded-mtext">Dashboard</span>
                    </a>
                </li>
                <li class="{{ active('order.*') }}">
                    <a href="{{ route('order.index') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fas fa-shopping-basket"></i>
                        </span>
                        <span class="pcoded-mtext">Orders</span>
                    </a>
                </li>
                <li class="{{ active('product.*') }}">
                    <a href="{{ route('product.index') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fas fa-tags"></i>
                        </span>
                        <span class="pcoded-mtext">Products</span>
                    </a>
                </li>
                <li class="{{ active('video.*') }}">
                    <a href="{{ route('video.index') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fab fa-youtube"></i>
                        </span>
                        <span class="pcoded-mtext">Video</span>
                    </a>
                </li>
            </ul>
            <div class="pcoded-navigation-label">Settings</div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="{{ active('category.*') }}">
                    <a href="{{ route('category.index') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fas fa-th-large"></i>
                        </span>
                        <span class="pcoded-mtext">Manage Category</span>
                    </a>
                </li>
                <li class="{{ active('subcategory.*') }}">
                    <a href="{{ route('subcategory.index') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fas fa-th"></i>
                        </span>
                        <span class="pcoded-mtext">Manage Sub-Category</span>
                    </a>
                </li>
                <li class="{{ active('user.*') }}">
                    <a href="{{ route('user.index') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fa fa-users"></i>
                        </span>
                        <span class="pcoded-mtext">Manage Users</span>
                    </a>
                </li>
                <li class="{{ active('role.*') }}">
                    <a href="{{ route('role.index') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fa fa-user-tag"></i>
                        </span>
                        <span class="pcoded-mtext">Manage Roles</span>
                    </a>
                </li>
                <li class="{{ active('setting.*') }}">
                    <a href="{{ route('setting.index') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fa fa-wrench"></i>
                        </span>
                        <span class="pcoded-mtext">Settings</span>
                    </a>
                </li>
            </ul>
            <div class="pcoded-navigation-label"></div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="{{ active('activity') }}">
                    <a href="{{ route('activity') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fa fa-th-list"></i>
                        </span>
                        <span class="pcoded-mtext">Activity History</span>
                    </a>
                </li>
                <li class="{{ active('login_history.*') }}">
                    <a href="{{ route('login_history.index') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fa fa-user-clock"></i>
                        </span>
                        <span class="pcoded-mtext">Login History</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('logout') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fa fa-sign-out-alt"></i>
                        </span>
                        <span class="pcoded-mtext">Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
