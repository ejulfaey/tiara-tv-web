@component('dashboard.layouts.app')
    @slot('title')
        Settings
    @endslot
    @slot('custom_css')
        <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fa fa-wrench bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Settings</h5>
                            <span>Manage General Setting</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('setting.index') }}">Settings</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>General Setting</h5>
                                            </div>
                                            <div class="card-block">
                                                <form method="post" id="basic_form" action="{{ route('setting.basic') }}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="title">Title</label>
                                                        <input type="text" id="title" name="title" placeholder="Enter Title" value="{{ getSetting('title') }}" class="form-control @if($errors->has('title')) is-invalid @endif">
                                                        @if($errors->has('title'))<div class="invalid-feedback">{{ $errors->first('title') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="description">Description</label>
                                                        <textarea name="description" id="description" rows="5" class="form-control">{{ getSetting('description') }}</textarea>
                                                        @if($errors->has('description'))<div class="invalid-feedback">{{ $errors->first('description') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="text" id="email" name="email" placeholder="Enter email" value="{{ getSetting('email') }}" class="form-control @if($errors->has('email')) is-invalid @endif">
                                                        @if($errors->has('email'))<div class="invalid-feedback">{{ $errors->first('email') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="contact_no">Contact Number</label>
                                                        <input type="text" id="contact_no" name="contact_no" placeholder="Enter contact_no" value="{{ getSetting('contact_no') }}" class="form-control @if($errors->has('contact_no')) is-invalid @endif">
                                                        @if($errors->has('contact_no'))<div class="invalid-feedback">{{ $errors->first('contact_no') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="address">Address</label>
                                                        <textarea name="address" id="address" rows="5" class="form-control">{{ getSetting('address') }}</textarea>
                                                        @if($errors->has('address'))<div class="invalid-feedback">{{ $errors->first('address') }}</div>@endif
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="card-footer">
                                                <input type="submit" form="basic_form" value="Submit" class="btn btn-primary">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Social Connnection</h5>
                                            </div>
                                            <div class="card-block">
                                                <form method="post" id="social_form" action="{{ route('setting.social') }}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="facebook">Facebook</label>
                                                        <input type="text" id="facebook" name="facebook" placeholder="Enter facebook" value="{{ getSetting('facebook') }}" class="form-control @if($errors->has('facebook')) is-invalid @endif">
                                                        @if($errors->has('facebook'))<div class="invalid-feedback">{{ $errors->first('facebook') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="twitter">Twitter</label>
                                                        <input type="text" id="twitter" name="twitter" placeholder="Enter twitter" value="{{ getSetting('twitter') }}" class="form-control @if($errors->has('twitter')) is-invalid @endif">
                                                        @if($errors->has('twitter'))<div class="invalid-feedback">{{ $errors->first('twitter') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="instagram">Instagram</label>
                                                        <input type="text" id="instagram" name="instagram" placeholder="Enter instagram" value="{{ getSetting('instagram') }}" class="form-control @if($errors->has('instagram')) is-invalid @endif">
                                                        @if($errors->has('instagram'))<div class="invalid-feedback">{{ $errors->first('instagram') }}</div>@endif
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="card-footer">
                                                <input type="submit" form="social_form" value="Submit" class="btn btn-primary">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Payment Gateway</h5>
                                            </div>
                                            <div class="card-block">
                                                <form method="post" id="payment_form" action="{{ route('setting.payment') }}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="currency">Currency</label>
                                                        <input type="text" id="currency" name="currency" placeholder="Enter currency" value="{{ getSetting('currency') }}" class="form-control @if($errors->has('currency')) is-invalid @endif">
                                                        @if($errors->has('currency'))<div class="invalid-feedback">{{ $errors->first('currency') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="min_donation">Minimum Donation</label>
                                                        <input type="text" id="min_donation" name="min_donation" placeholder="Enter min_donation" value="{{ getSetting('min_donation') }}" class="form-control @if($errors->has('min_donation')) is-invalid @endif">
                                                        @if($errors->has('min_donation'))<div class="invalid-feedback">{{ $errors->first('min_donation') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="sandbox">Use Sandbox?</label>
                                                        <select name="sandbox" id="sandbox" class="form-control @if($errors->has('sandbox')) is-invalid @endif">
                                                            <option value="yes" @if(getSetting('sandbox') == "yes") selected @endif>Yes</option>
                                                            <option value="no" @if(getSetting('sandbox') == "no") selected @endif>No</option>
                                                        </select>
                                                        @if($errors->has('sandbox'))<div class="invalid-feedback">{{ $errors->first('sandbox') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="billplz_secret_key">BillPlz Secret Key</label>
                                                        <input type="text" id="billplz_secret_key" name="billplz_secret_key" placeholder="Enter billplz_secret_key" value="{{ getSetting('billplz_secret_key') }}" class="form-control @if($errors->has('billplz_secret_key')) is-invalid @endif">
                                                        @if($errors->has('billplz_secret_key'))<div class="invalid-feedback">{{ $errors->first('billplz_secret_key') }}</div>@endif
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="card-footer">
                                                <input type="submit" form="payment_form" value="Submit" class="btn btn-primary">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Logo & Icons</h5>
                                            </div>
                                            <div class="card-block">
                                                <form method="post" id="logo_form" action="{{ route('setting.icon') }}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="main_logo">Main Logo (200px x 45px)</label>
                                                        <input type="file" name="main_logo" id="main_logo" class="dropify" data-default-file="{{ getSetting('main_logo') }}">
                                                        @if($errors->has('main_logo'))<div class="invalid-feedback">{{ $errors->first('main_logo') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="small_logo">Small Logo (64px x 64px)</label>
                                                        <input type="file" name="small_logo" id="small_logo" class="dropify" data-default-file="{{ getSetting('small_logo') }}">
                                                        @if($errors->has('small_logo'))<div class="invalid-feedback">{{ $errors->first('small_logo') }}</div>@endif
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="card-footer">
                                                <input type="submit" form="logo_form" value="Submit" class="btn btn-primary">
                                            </div>
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Banner Slider - 1950px x 750px</h5>
                                            </div>
                                            <div class="card-block">
                                                <form method="post" id="slider_form" action="{{ route('setting.slider') }}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="section_bg">Section Background</label>
                                                        <input type="file" name="section_bg" id="section_bg" class="dropify" data-default-file="{{ getSetting('section_bg') }}">
                                                        @if($errors->has('section_bg'))<div class="invalid-feedback">{{ $errors->first('section_bg') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="slider_1">Slider 1</label>
                                                        <input type="file" name="slider_1" id="slider_1" class="dropify" data-default-file="{{ getSetting('slider_1') }}" data-id="slider_1">
                                                        @if($errors->has('slider_1'))<div class="invalid-feedback">{{ $errors->first('slider_1') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="slider_2">Slider 2</label>
                                                        <input type="file" name="slider_2" id="slider_2" class="dropify" data-default-file="{{ getSetting('slider_2') }}" data-id="slider_2">
                                                        @if($errors->has('slider_2'))<div class="invalid-feedback">{{ $errors->first('slider_2') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="slider_3">Slider 3</label>
                                                        <input type="file" name="slider_3" id="slider_3" class="dropify" data-default-file="{{ getSetting('slider_3') }}" data-id="slider_3">
                                                        @if($errors->has('slider_3'))<div class="invalid-feedback">{{ $errors->first('slider_3') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="slider_4">Slider 4</label>
                                                        <input type="file" name="slider_4" id="slider_4" class="dropify" data-default-file="{{ getSetting('slider_4') }}" data-id="slider_4">
                                                        @if($errors->has('slider_4'))<div class="invalid-feedback">{{ $errors->first('slider_4') }}</div>@endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="slider_5">Slider 5</label>
                                                        <input type="file" name="slider_5" id="slider_5" class="dropify" data-default-file="{{ getSetting('slider_5') }}" data-id="slider_5">
                                                        @if($errors->has('slider_5'))<div class="invalid-feedback">{{ $errors->first('slider_5') }}</div>@endif
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="card-footer">
                                                <input type="submit" form="slider_form" value="Submit" class="btn btn-primary">
                                            </div>
                                        </div>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
    @slot('custom_js')
    <script src="{{ asset('js/dropify.min.js')}}"></script>
    <script>
        var drEvent = $('.dropify').dropify();
        drEvent.on('dropify.afterClear', function(event, element){
            deleteSlider(element.settings.id);
        });
        function deleteSlider(id){
            console.log(id);
            $.ajax({
                url: '/ajax/delete_slider/' + id,
                type: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(res) {
                    console.log(res);
                    if(res.status == 200) {
                        location.reload();
                    }
                    if(res.status == 404) {
                        swal({
                            title: "Cannot remove",
                            text: "At least 1 image need to stay",
                            type: 'error',
                            allowOutsideClick: () => !swal.isLoading()
                        }).then((result) => {
                            location.reload();
                        });
                    }
                }
            });
        }

    </script>
    @endslot
@endcomponent