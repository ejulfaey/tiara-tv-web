@component('dashboard.layouts.app')
@slot('title')
Create Category
@endslot
@slot('custom_css')
<link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
@endslot
@slot('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="fa fa-th-large bg-c-blue"></i>
                <div class="d-inline">
                    <h5>Create Category</h5>
                    <span>New Category</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class=" breadcrumb breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('category.index') }}">All Categories</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">New Category</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <h5>Create Form</h5>
                            </div>
                            <div class="card-block">
                                <form method="post" id="form" action="{{ route('category.store') }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Name <span class="required">*</span></label>
                                        <input type="text" id="name" name="name" placeholder="Enter Name"
                                            value="{{ old('name') }}"
                                            class="form-control @if($errors->has('name')) is-invalid @endif">
                                        @if($errors->has('name'))<div class="invalid-feedback">
                                            {{ $errors->first('name') }}</div>@endif
                                    </div>
                                    <div class="form-group">
                                        <label for="desc">Description</label>
                                        <textarea name="desc" id="desc" rows="7" placeholder="Enter Description"
                                            class="form-control @if($errors->has('desc')) is-invalid @endif">{{ old('desc') }}</textarea>
                                        @if($errors->has('desc'))<div class="invalid-feedback">
                                            {{ $errors->first('desc') }}</div>@endif
                                    </div>
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" name="image" id="image" class="dropify">
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Status <span class="required">*</span></label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                        @if($errors->has('status'))<div class="invalid-feedback">
                                            {{ $errors->first('status') }}</div>@endif
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('category.index') }}" class="btn btn-light">Back</a>
                                <input type="submit" form="form" value="Submit" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endslot
@slot('custom_js')
<script src="{{ asset('js/dropify.min.js') }}"></script>
<script>
    $('.dropify').dropify();
</script>
@endslot
@endcomponent