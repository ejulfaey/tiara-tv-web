@component('dashboard.layouts.app')
@slot('title')
Update Category
@endslot
@slot('custom_css')
<link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
@endslot
@slot('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="fa fa-th-large bg-c-blue"></i>
                <div class="d-inline">
                    <h5>Update Category</h5>
                    <span>{{ $category->name }}</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class=" breadcrumb breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('category.index') }}">All Categories</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">Update Category</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <h5>Update Form</h5>
                            </div>
                            <div class="card-block">
                                <form method="post" id="form" action="{{ route('category.update', $category) }}" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label for="name">Name <span class="required">*</span></label>
                                        <input type="text" id="name" name="name" placeholder="Enter Name" value="{{ $category->name }}" class="form-control @if($errors->has('name')) is-invalid @endif">
                                        @if($errors->has('name'))<div class="invalid-feedback">{{ $errors->first('name') }}</div>@endif
                                    </div>
                                    <div class="form-group">
                                        <label for="desc">Description</label>
                                        <textarea name="desc" id="desc" rows="7" placeholder="Enter Description" class="form-control @if($errors->has('desc')) is-invalid @endif">{{ $category->desc }}</textarea>
                                        @if($errors->has('desc'))<div class="invalid-feedback">{{ $errors->first('desc') }}</div>@endif
                                    </div>
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" name="image" id="image" class="dropify" data-default-file="{{ $category->image == null ? '' : url($category->image) }}" data-show-remove="false">
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Status <span class="required">*</span></label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="1" @if($category->status == 1) selected @endif>Active</option>
                                            <option value="0" @if($category->status == 0) selected @endif>Inactive</option>
                                        </select>
                                        @if($errors->has('status'))<div class="invalid-feedback">{{ $errors->first('status') }}</div>@endif
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('category.index') }}" class="btn btn-light">Back</a>
                                <input type="submit" form="form" value="Submit" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endslot
@slot('custom_js')
<script src="{{ asset('js/dropify.min.js') }}"></script>
<script>
    $('.dropify').dropify();
</script>
@endslot
@endcomponent