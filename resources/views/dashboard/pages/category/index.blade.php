@component('dashboard.layouts.app')
    @slot('title')
        Manage Category
    @endslot
    @slot('custom_css')
        <link rel="stylesheet" href="{{ asset('css/datatables.min.css?id=' . str_random(5) ) }}">
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fa fa-th-large bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Manage Category</h5>
                            <span>Manage All Categories</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">All Categories</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Categories List</h5>
                                        <a href="{{ route('category.create') }}" class="btn btn-sm btn-primary float-right">
                                            <i class="fa fa-plus"></i>
                                            Category
                                        </a>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="dataTable" class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th></th>
                                                        <th>Name</th>
                                                        <th>Description</th>
                                                        <th class="text-center">Status</th>
                                                        <th class="text-right">Date</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($categories as $category)
                                                        <tr>
                                                            <td align="center">{{ $loop->iteration }}</td>
                                                            <td align="center">
                                                                <img src="{{ $category->image == null ? asset('icons/avatar.png') : $category->image }}" alt="" width="30">
                                                            </td>
                                                            <td>{{ $category->name }}</td>
                                                            <td>{{ $category->desc == null ? 'N/A' : str_limit($category->desc, 40) }}</td>
                                                            <td align="center">
                                                                <label class="label {{ $category->status == 1 ? 'label-success' : 'label-danger' }}">
                                                                    {{ $category->status == 1 ? 'Active' : 'Inactive' }}
                                                                </label>
                                                            </td>
                                                            <td align="right">{{ getDefaultDate($category->created_at) }}</td>
                                                            <td align="right">
                                                                <a href="{{ route('category.edit', $category) }}" class="btn btn-mini btn-warning">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                                <a href="" id="btnDelete" data-id="{{ $category->id }}" data-func="category" class="btn btn-mini btn-danger">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </td>
                                                        </tr> 
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th></th>
                                                        <th>Name</th>
                                                        <th>Description</th>
                                                        <th class="text-center">Status</th>
                                                        <th class="text-right">Date</th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
    @slot('custom_js')
        <script src="{{ asset('js/function.js?id=' . str_random(5)) }}"></script>
        <script src="{{ asset('js/datatables.min.js?id=' . str_random(5)) }}"></script>
        <script>
            $('#dataTable').DataTable({});
        </script>
    @endslot
@endcomponent