@component('dashboard.layouts.app')
    @slot('title')
        Create Role
    @endslot
    @slot('custom_css')
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fa fa-user-tag bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Create Role</h5>
                            <span>Define Role</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('role.index') }}">All Roles</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('role.create') }}">Create Role</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Create Form</h5>
                                    </div>
                                    <div class="card-block">
                                        <form method="post" id="form" action="{{ route('role.store') }}">
                                            @csrf
                                            <div class="form-group">
                                                <label for="name">Name <span class="required">*</span></label>
                                                <input type="text" id="name" name="name" placeholder="Enter Name" value="{{ old('name') }}" class="form-control @if($errors->has('name')) is-invalid @endif">
                                                @if($errors->has('name'))<div class="invalid-feedback">{{ $errors->first('name') }}</div>@endif
                                            </div>
                                            <div class="form-group">
                                                <label for="code">Code <span class="required">*</span></label>
                                                <input type="text" id="code" name="code" readonly value="{{ old('code') }}" class="form-control @if($errors->has('code')) is-invalid @endif">
                                                @if($errors->has('code'))<div class="invalid-feedback">{{ $errors->first('code') }}</div>@endif
                                            </div>
                                            <div class="form-group">
                                                <label for="status">Status <span class="required">*</span></label>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                                @if($errors->has('status'))<div class="invalid-feedback">{{ $errors->first('status') }}</div>@endif
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <a href="{{ route('role.index') }}" class="btn btn-light">Back</a>
                                        <input type="submit" form="form" value="Submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
    @slot('custom_js')
        <script>

            $("#name").keyup(function(res){

                var str = res.target.value;
                str = str.replace(/ /g, "-");
                str = str.toLowerCase();
                
                $("#code").val(str);

            });
        </script>
    @endslot
@endcomponent