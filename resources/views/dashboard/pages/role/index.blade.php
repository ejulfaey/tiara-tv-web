@component('dashboard.layouts.app')
    @slot('title')
        Manage Roles
    @endslot
    @slot('custom_css')
        <link rel="stylesheet" href="{{ asset('css/datatables.min.css?id=' . str_random(5) ) }}">
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fa fa-user-tag bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Roles</h5>
                            <span>Manage all Roles</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('role.index') }}">All Roles</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Role List</h5>
                                        <a href="{{ route('role.create') }}" class="btn btn-sm btn-primary float-right">
                                            <i class="fa fa-plus"></i>
                                            role
                                        </a>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="dataTable" class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th>Name</th>
                                                        <th>Code</th>
                                                        <th class="text-center">Status</th>
                                                        <th class="text-right">Date</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($roles as $role)
                                                        <tr>
                                                            <td align="center">{{ $loop->iteration }}</td>
                                                            <td>{{ $role->name }}</td>
                                                            <td>{{ $role->code }}</td>
                                                            <td align="center">
                                                                <label class="label {{ $role->status == 1 ? 'label-success' : 'label-danger' }}">
                                                                    {{ $role->status == 1 ? 'Active' : 'Inactive' }}
                                                                </label>
                                                            </td>
                                                            <td align="right">{{ getDefaultDate($role->created_at) }}</td>
                                                            <td align="right">
                                                                <a href="{{ route('role.edit', $role) }}" class="btn btn-mini btn-warning">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                                <a href="" id="btnDelete" data-id="{{ $role->id }}" data-func="role" class="btn btn-mini btn-danger">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </td>
                                                        </tr> 
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th>Name</th>
                                                        <th>Code</th>
                                                        <th class="text-center">Status</th>
                                                        <th class="text-right">Date</th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
    @slot('custom_js')
        <script src="{{ asset('js/function.js?id=' . str_random(5)) }}"></script>
        <script src="{{ asset('js/datatables.min.js?id=' . str_random(5)) }}"></script>
        <script>
            $('#dataTable').DataTable({});
        </script>
    @endslot
@endcomponent