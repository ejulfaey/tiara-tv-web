@component('dashboard.layouts.app')
    @slot('title')
        Update Product
    @endslot
    @slot('custom_css')
        <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fas fa-tags bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Update Product</h5>
                            <span>{{ $product->code . ' - ' . $product->name }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('product.index') }}">All Products</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Edit Product</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Create Form</h5>
                                    </div>
                                    <div class="card-block">
                                        <form method="post" id="form" action="{{ route('product.update', $product) }}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="name">Name <span class="required">*</span></label>
                                                    <input type="text" id="name" name="name" placeholder="Enter Name" value="{{ $product->name }}" class="form-control @if($errors->has('name')) is-invalid @endif">
                                                    @if($errors->has('name'))<div class="invalid-feedback">{{ $errors->first('name') }}</div>@endif
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="sku">SKU</label>
                                                    <input type="text" id="sku" name="sku" placeholder="Enter SKU Number" value="{{ $product->sku }}" class="form-control @if($errors->has('sku')) is-invalid @endif">
                                                    @if($errors->has('sku'))<div class="invalid-feedback">{{ $errors->first('sku') }}</div>@endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="category_id">Category <span class="required">*</span></label>
                                                    <select name="category_id" id="category_id" class="form-control @if($errors->has('category_id')) is-invalid @endif">
                                                        <option value="">Select Category</option>
                                                        @foreach(getCategories() as $category)
                                                            @if(old('category_id') == $category->id)
                                                                <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                                            @else
                                                                <option value="{{ $category->id }}" @if($product->category_id == $category->id) selected @endif>{{ $category->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has('category_id'))<div class="invalid-feedback">{{ $errors->first('category_id') }}</div>@endif
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="subcategory_id">Sub Category</label>
                                                    <select name="subcategory_id" id="subcategory_id" class="form-control @if($errors->has('subcategory_id')) is-invalid @endif">
                                                        <option value="">Select Sub Category</option>
                                                        @foreach(getSubCategories() as $category)
                                                            @if(old('subcategory_id') == $category->id)
                                                                <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                                            @else
                                                                <option value="{{ $category->id }}" @if($product->subcategory_id == $category->id) selected @endif>{{ $category->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has('subcategory_id'))<div class="invalid-feedback">{{ $errors->first('subcategory_id') }}</div>@endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="price">Price <span class="required">*</span></label>
                                                    <input type="number" id="price" name="price" step="0.01" placeholder="Enter Price" value="{{ $product->price }}" class="form-control @if($errors->has('price')) is-invalid @endif">
                                                    @if($errors->has('price'))<div class="invalid-feedback">{{ $errors->first('price') }}</div>@endif
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="tags">Tags </label>
                                                    <input type="text" id="tags" name="tags" placeholder="Enter Tags" value="{{ $product->tags }}" class="form-control @if($errors->has('tags')) is-invalid @endif">
                                                    @if($errors->has('tags'))<div class="invalid-feedback">{{ $errors->first('tags') }}</div>@endif
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="desc">Description</label>
                                                    <textarea name="desc" id="desc" rows="5" placeholder="Enter Description" class="form-control @if($errors->has('desc')) is-invalid @endif">{{ $product->desc }}</textarea>
                                                    @if($errors->has('desc'))<div class="invalid-feedback">{{ $errors->first('desc') }}</div>@endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <label for="image">Upload New Photos</label>
                                                    <input type="file" name="image[]" id="image" class="dropify" multiple>
                                                </div>
                                                @forelse($product->images as $image)
                                                    <div class="form-group col-md-3">
                                                        <label for="pictures">Photo - {{ $loop->iteration }}</label>
                                                        <input type="file" name="image[]" id="image" class="dropify" data-default-file="{{ $image->image }}" data-id="{{ $image->id }}">
                                                    </div>
                                                @empty
                                                @endforelse
                                            </div>
                                            <div class="form-group">
                                                <label for="status">Status <span class="required">*</span></label>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                                @if($errors->has('status'))<div class="invalid-feedback">{{ $errors->first('status') }}</div>@endif
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <a href="{{ route('product.index') }}" class="btn btn-light">Back</a>
                                        <input type="submit" form="form" value="Submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
    @slot('custom_js')
    <script src="{{ asset('js/dropify.min.js')}}"></script>
    <script>
        $('.dropify').dropify();
        var drEvent = $('.dropify').dropify();

        drEvent.on('dropify.afterClear', function(event, element){
            deletePicture(element.settings.id);
        });
        function deletePicture(id){
            $.ajax({
                url: '/ajax/delete_photo/' + id,
                type: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(res) {
                    console.log(res);
                    if(res.status == 200) {
                        location.reload();
                    }
                    if(res.status == 404) {
                        swal({
                            title: "Cannot remove",
                            text: "At least 1 image need to stay",
                            type: 'error',
                            allowOutsideClick: () => !swal.isLoading()
                        }).then((result) => {
                            location.reload();
                        });
                    }
                }
            });
        }
    </script>
    @endslot
@endcomponent