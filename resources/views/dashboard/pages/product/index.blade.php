@component('dashboard.layouts.app')
@slot('title')
Manage All Products
@endslot
@slot('custom_css')
<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">
@endslot
@slot('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="fas fa-tags bg-c-blue"></i>
                <div class="d-inline">
                    <h5>Products</h5>
                    <span>Manage All Products</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class=" breadcrumb breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('dashboard') }}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">All Products</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Products List</h5>
                                <a href="{{ route('product.create') }}" class="btn btn-sm btn-primary float-right">
                                    <i class="fa fa-plus"></i>
                                    Product
                                </a>
                            </div>
                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table id="dataTable" class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th></th>
                                                <th>Name</th>
                                                <th>SKU</th>
                                                <th>Category</th>
                                                <th>Sub Category</th>
                                                <th class="text-right">Price</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-right">Date</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($products as $product)
                                            <tr>
                                                <td align="center">{{ $loop->iteration }}</td>
                                                <td>
                                                    <img src="{{ url($product->images[0]->image) }}" alt="" width="100%">
                                                </td>
                                                <td>{{ $product->name }}</td>
                                                <td>{{ $product->sku }}</td>
                                                <td>{{ $product->category->name }}</td>
                                                <td>{{ $product->subcategory == null ? 'N/A' : $product->subcategory->name }}</td>
                                                <td align="right">{{ $product->price }}</td>
                                                <td align="center">
                                                    <label class="label {{ $product->status == 1 ? 'label-success' : 'label-danger' }}">
                                                        {{ $product->status == 1 ? 'Active' : 'Inactive' }}
                                                    </label>
                                                </td>
                                                <td align="right">{{ getDefaultDate($product->created_at) }}</td>
                                                <td align="right">
                                                    <a href="{{ route('product.edit', $product) }}" class="btn btn-mini btn-warning">
                                                        <i class="fa fa-pencil-alt"></i>
                                                    </a>
                                                    <a href="" id="btnDelete" data-id="{{ $product->id }}" data-func="product" class="btn btn-mini btn-danger">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th></th>
                                                <th>Name</th>
                                                <th>SKU</th>
                                                <th>Category</th>
                                                <th>Sub Category</th>
                                                <th class="text-right">Price</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-right">Date</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endslot
@slot('custom_js')
<script src="{{ asset('js/function.js?id=' . str_random(5)) }}"></script>
<script src="{{ asset('js/datatables.min.js?id=' . str_random(5)) }}"></script>
<script>
    $('#dataTable').DataTable({});
</script>
@endslot
@endcomponent