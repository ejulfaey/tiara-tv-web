@component('dashboard.layouts.app')
    @slot('title')
        View Login History
    @endslot
    @slot('custom_css')
        <link rel="stylesheet" href="{{ asset('css/datatables.min.css?id=' . str_random(5) ) }}">
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fa fa-user-clock bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>View Login History</h5>
                            <span>Track history</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('login_history.index') }}">Login History</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Login History</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="dataTable" class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th class="text-center"></th>
                                                        <th>User</th>
                                                        <th>IP Address</th>
                                                        <th class="text-right">Time</th>
                                                        <th class="text-right">Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($logins as $login)
                                                        <tr>
                                                            <td align="center">{{ $loop->iteration }}</td>
                                                            <td align="center">
                                                                <img src="{{ $login->user->image }}" alt="" width="30">
                                                            </td>
                                                            <td>{{ $login->user->fullname }}</td>
                                                            <td>{{ $login->ip_address }}</td>
                                                            <td align="right">{{ getDefaultTime($login->created_at) }}</td>
                                                            <td align="right">{{ getDefaultDate($login->created_at) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th class="text-center"></th>
                                                        <th>User</th>
                                                        <th>IP Address</th>
                                                        <th class="text-right">Time</th>
                                                        <th class="text-right">Date</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
    @slot('custom_js')
        <script src="{{ asset('js/function.js?id=' . str_random(5)) }}"></script>
        <script src="{{ asset('js/datatables.min.js?id=' . str_random(5)) }}"></script>
        <script>
            $('#dataTable').DataTable({});
        </script>
    @endslot
@endcomponent