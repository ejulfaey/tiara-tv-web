@component('dashboard.layouts.app')
    @slot('title')
        Create Video
    @endslot
    @slot('custom_css')
        <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fas fa-tags bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Create Video</h5>
                            <span>New Video</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('video.index') }}">All Video</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">New Video</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Create Form</h5>
                                    </div>
                                    <div class="card-block">
                                        <form method="post" id="form" action="{{ route('video.store') }}" enctype="multipart/form-data">
                                            @csrf                                            
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="name">Name <span class="required">*</span></label>
                                                    <input type="text" id="name" name="name" placeholder="Enter Name" value="{{ old('name') }}" class="form-control @if($errors->has('name')) is-invalid @endif">
                                                    @if($errors->has('name'))<div class="invalid-feedback">{{ $errors->first('name') }}</div>@endif
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="link">Link <span class="required">*</span></label>
                                                    <input type="text" id="link" name="link" placeholder="Enter Link" value="{{ old('link') }}" class="form-control @if($errors->has('link')) is-invalid @endif">
                                                    @if($errors->has('link'))<div class="invalid-feedback">{{ $errors->first('link') }}</div>@endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="category_id">Category <span class="required">*</span></label>
                                                    <select name="category_id" id="category_id" class="form-control @if($errors->has('category_id')) is-invalid @endif">
                                                        <option value="">Select Category</option>
                                                        @foreach(getCategories() as $category)
                                                            @if(old('category_id') == $category->id)
                                                                <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                                            @else
                                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has('category_id'))<div class="invalid-feedback">{{ $errors->first('category_id') }}</div>@endif
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="subcategory_id">Sub Category</label>
                                                    <select name="subcategory_id" id="subcategory_id" class="form-control @if($errors->has('subcategory_id')) is-invalid @endif">
                                                        <option value="">Select Sub Category</option>
                                                        @foreach(getSubCategories() as $category)
                                                            @if(old('subcategory_id') == $category->id)
                                                                <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                                            @else
                                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has('subcategory_id'))<div class="invalid-feedback">{{ $errors->first('subcategory_id') }}</div>@endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="desc">Description</label>
                                                <textarea name="desc" id="desc" rows="5" placeholder="Enter Description" class="form-control @if($errors->has('desc')) is-invalid @endif"></textarea>
                                                @if($errors->has('desc'))<div class="invalid-feedback">{{ $errors->first('desc') }}</div>@endif
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <a href="{{ route('video.index') }}" class="btn btn-light">Back</a>
                                        <input type="submit" form="form" value="Submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
    @slot('custom_js')
    <script src="{{ asset('js/dropify.min.js')}}"></script>
    <script>
        $('.dropify').dropify();
    </script>
    @endslot
@endcomponent