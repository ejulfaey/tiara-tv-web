@component('dashboard.layouts.app')
    @slot('title')
        Manage All video
    @endslot
    @slot('custom_css')
        <link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fas fa-tags bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Video</h5>
                            <span>Manage All Videos</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">All Videos</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Video List</h5>
                                        <a href="{{ route('video.create') }}" class="btn btn-sm btn-primary float-right">
                                            <i class="fa fa-plus"></i>
                                            Video
                                        </a>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="dataTable" class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th>Name</th>
                                                        <th>Link</th>
                                                        <th>Category</th>
                                                        <th>Sub Category</th>
                                                        <th class="text-right">Desc</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($videos as $video)
                                                        <tr>
                                                            <td align="center">{{ $loop->iteration }}</td>
                                                            <td>{{ $video->name }}</td>
                                                            <td>{{ $video->link }}</td>
                                                            <td>{{ $video->category->name }}</td>
                                                            <td>{{ $video->subcategory == null ? 'N/A' : $video->subcategory->name }}</td>
                                                            <td align="right">{{ getDefaultDate($video->created_at) }}</td>
                                                            <td align="right">
                                                                <a href="{{ route('video.edit', $video) }}" class="btn btn-mini btn-warning">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                                <a href="" id="btnDelete" data-id="{{ $video->id }}" data-func="video" class="btn btn-mini btn-danger">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </td>
                                                        </tr> 
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th>Name</th>
                                                        <th>Link</th>
                                                        <th>Category</th>
                                                        <th>Sub Category</th>
                                                        <th class="text-right">Desc</th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
    @slot('custom_js')
        <script src="{{ asset('js/function.js?id=' . str_random(5)) }}"></script>
        <script src="{{ asset('js/datatables.min.js?id=' . str_random(5)) }}"></script>
        <script>
            $('#dataTable').DataTable({});
        </script>
    @endslot
@endcomponent