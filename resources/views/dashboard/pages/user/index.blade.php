@component('dashboard.layouts.app')
    @slot('title')
        Manage Users
    @endslot
    @slot('custom_css')
        <link rel="stylesheet" href="{{ asset('css/datatables.min.css?id=' . str_random(5) ) }}">
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fa fa-users bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Users</h5>
                            <span>Manage all users</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('user.index') }}">All Users</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>User List</h5>
                                        <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary float-right">
                                            <i class="fa fa-plus"></i>
                                            User
                                        </a>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="dataTable" class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th></th>
                                                        <th>Full Name</th>
                                                        <th>User Name</th>
                                                        <th class="text-center">Role</th>
                                                        <th class="text-right">Last Login</th>
                                                        <th class="text-right">Created At</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($users as $user)
                                                        <tr>
                                                            <td align="center">{{ $loop->iteration }}</td>
                                                            <td align="center"><img src="{{ url($user->image) }}" width="30"></td>
                                                            <td>{{ $user->fullname }}</td>
                                                            <td>{{ $user->username }}</td>
                                                            <td align="center">
                                                                <label class="label {{ $user->role == 'admin' ? 'label-primary' : 'label-warning'}}">
                                                                    {{ $user->roles->name }}
                                                                </label>
                                                            </td>
                                                            <td align="right">{{ $user->login_at == null ? 'N/A' : getHumanDate($user->login_at) }}</td>
                                                            <td align="right">{{ getDefaultDate($user->created_at) }}</td>
                                                            <td align="right">
                                                                <a href="{{ route('user.edit', $user) }}" class="btn btn-mini btn-warning">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                                <a href="" id="btnDelete" data-id="{{ $user->id }}" data-func="user" class="btn btn-mini btn-danger">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </td>
                                                        </tr> 
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th></th>
                                                        <th>Full Name</th>
                                                        <th>Email</th>
                                                        <th class="text-center">Role</th>
                                                        <th class="text-right">Last Login</th>
                                                        <th class="text-right">Created At</th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
    @slot('custom_js')
        <script src="{{ asset('js/function.js?id=' . str_random(5)) }}"></script>
        <script src="{{ asset('js/datatables.min.js?id=' . str_random(5)) }}"></script>
        <script>
            $('#dataTable').DataTable({
                "columnDefs": [{
                    "type": "date-dd-mmm-yyyy", targets: 6,
                    "type": "date-dd-mmm-yyyy", targets: 7
                }]
            });
        </script>
    @endslot
@endcomponent