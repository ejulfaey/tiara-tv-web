@component('dashboard.layouts.app')
    @slot('title')
        Update User
    @endslot
    @slot('custom_css')
        <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fa fa-users bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Update User</h5>
                            <span>Update - {{ $user->fullname }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('user.index') }}">All Users</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('user.update', $user) }}">Update User</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Update Form</h5>
                                    </div>
                                    <div class="card-block">
                                        <form method="post" id="form" action="{{ route('user.update', $user) }}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="fullname">Full Name <span class="required">*</span></label>
                                                    <input type="text" id="fullname" name="fullname" placeholder="Enter Full Name" value="{{ $user->fullname }}" class="form-control @if($errors->has('fullname')) is-invalid @endif">
                                                    @if($errors->has('fullname'))<div class="invalid-feedback">{{ $errors->first('fullname') }}</div>@endif
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="username">Username <span class="required">*</span></label>
                                                    <input type="text" id="username" name="username" placeholder="Enter User Name" value="{{ $user->username }}" readonly class="form-control @if($errors->has('username')) is-invalid @endif">
                                                    @if($errors->has('username'))<div class="invalid-feedback">{{ $errors->first('username') }}</div>@endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="password">Password <span class="required">*</span></label>
                                                    <input type="password" id="password" name="password" placeholder="Enter Password" class="form-control @if($errors->has('password')) is-invalid @endif">
                                                    @if($errors->has('password'))<div class="invalid-feedback">{{ $errors->first('password') }}</div>@endif
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="password_confirmation">Re-enter Password <span class="required">*</span></label>
                                                    <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password" class="form-control @if($errors->has('password_confirmation')) is-invalid @endif">
                                                    @if($errors->has('password_confirmation'))<div class="invalid-feedback">{{ $errors->first('password_confirmation') }}</div>@endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="role">Role Type <span class="required">*</span></label>
                                                    <select name="role" id="role" class="form-control @if($errors->has('role')) is-invalid @endif">
                                                        <option value="">Select Role</option>
                                                        @foreach(getRoles() as $role)
                                                            @if(old('role') == $role->code)
                                                                <option value="{{ $role->code }}" selected>{{ $role->name }}</option>
                                                            @else
                                                                <option value="{{ $role->code }}" @if($user->role == $role->code) selected @endif>{{ $role->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="image">Profile Picture</label>
                                                    <input type="file" name="image" id="image" class="dropify" data-default-file="{{ $user->image }}">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <a href="{{ route('user.index') }}" class="btn btn-light">Back</a>
                                        <input type="submit" form="form" value="Submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
    @slot('custom_js')
    <script src="{{ asset('js/dropify.min.js')}}"></script>
    <script>
        $('.dropify').dropify();
    </script>
    @endslot
@endcomponent