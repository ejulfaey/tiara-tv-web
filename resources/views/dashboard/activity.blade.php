@component('dashboard.layouts.app')
    @slot('title')
        Activity List
    @endslot
    @slot('custom_css')
        <link rel="stylesheet" href="{{ asset('css/datatables.min.css?id=' . str_random(5) ) }}">
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fa fa-th-list bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Activities</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('activity') }}">Activities</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Activity List</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="dataTable" class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th></th>
                                                        <th>User</th>
                                                        <th class="text-center">Action</th>
                                                        <th>Description</th>
                                                        <th class="text-center">Time</th>
                                                        <th class="text-center">Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($activities as $activity)
                                                        <tr>
                                                            <td align="center">{{ $loop->iteration }}</td>
                                                            <td>
                                                                <img src="{{ $activity->user->image }}" alt="" class="img-circle">
                                                            </td>
                                                            <td>{{ $activity->user->fullname }}</td>
                                                            <td align="center">
                                                                @php
                                                                    if($activity->action == "create") {
                                                                        $label = 'label-success';
                                                                    } else if ($activity->action == "update") {
                                                                        $label = 'label-warning';
                                                                    } else {
                                                                        $label = 'label-danger';
                                                                    }
                                                                @endphp
                                                                <label class="label {{ $label }}">
                                                                    {{ strtoupper($activity->action) }}
                                                                </label>
                                                            </td>
                                                            <td>{{ $activity->description }}</td>
                                                            <td align="right">{{ date('H:i A', strtotime($activity->created_at)) }}</td>
                                                            <td align="right">{{ getDefaultDate($activity->created_at) }}</td>
                                                        </tr> 
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th></th>
                                                        <th>User</th>
                                                        <th class="text-center">Action</th>
                                                        <th>Description</th>
                                                        <th class="text-center">Time</th>
                                                        <th class="text-center">Date</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
    @slot('custom_js')
        <script src="{{ asset('js/function.js?id=' . str_random(5)) }}"></script>
        <script src="{{ asset('js/datatables.min.js?id=' . str_random(5)) }}"></script>
        <script>
            $('#dataTable').DataTable({});
        </script>
    @endslot
@endcomponent