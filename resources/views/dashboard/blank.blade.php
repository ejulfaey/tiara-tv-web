@component('dashboard.layouts.app')
        @slot('title')
    Blank
    @endslot
    @slot('custom_css')
    @endslot
    @slot('content')
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fa fa-home bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Blank</h5>
                            <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="index.html"><i class="fa fa-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    @endslot
    @slot('custom_js')
    @endslot
@endcomponent