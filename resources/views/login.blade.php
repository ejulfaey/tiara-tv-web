<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ getSetting('title') }} | Sign In</title>
    <link rel="stylesheet" href="{{ asset('themes/admindek/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/login.css?id=' . str_random(5)) }}">
</head>

<body>
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
            <div class="col-md-8 col-lg-6">
                <div class="login d-flex align-items-center py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-lg-8 mx-auto">
                                <h3 class="login-heading mb-4">Welcome back!</h3>
                                <form method="post" action="{{ route('login.post') }}">
                                    @csrf
                                    <div class="form-label-group">
                                        <input type="text" name="username" id="username" placeholder="Username" class="form-control @if($errors->has('username')) is-invalid @endif">
                                        <label for="username">Username</label>
                                        @if($errors->has('username'))<div class="invalid-feedback text-center">{{ $errors->first('username') }}</div>@endif
                                    </div>
                                    <div class="form-label-group">
                                        <input type="password" name="password" id="password" placeholder="Password" class="form-control @if($errors->has('password')) is-invalid @endif">
                                        <label for="password">Password</label>
                                        @if($errors->has('password'))<div class="invalid-feedback text-center">{{ $errors->first('password') }}</div>@endif
                                    </div>
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Remember password</label>
                                    </div>
                                    <button
                                        class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2"
                                        type="submit">Sign in</button>
                                    <div class="text-center">
                                    <a class="small" href="#">Forgot password?</a></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>