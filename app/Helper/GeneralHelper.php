<?php

/**
 * Get settings
 *
 * * @param code
 *
 * @return collection
 */
if (! function_exists('getSetting')) {
    function getSetting($code)
    {
        $setting = App\Models\Setting::whereCode($code)
        ->first();

        if($setting){
            return $setting->value;
        } else {
            return $code;
        }
    }
}

/**
 * Get all roles
 *
 * * @param none
 *
 * @return collection
 */
if (! function_exists('getRoles')) {
    function getRoles()
    {
        return App\Models\Role::whereStatus(1)
        ->orderBy('name')
        ->get();
    }
}

/**
 * Get all categories
 *
 * * @param none
 *
 * @return collection
 */
if (! function_exists('getCategories')) {
    function getCategories()
    {
        return App\Models\Category::whereStatus(1)
        ->orderBy('name')
        ->get();
    }
}

/**
 * Get all sub-categories
 *
 * * @param none
 *
 * @return collection
 */
if (! function_exists('getSubCategories')) {
    function getSubCategories()
    {
        return App\Models\SubCategory::whereStatus(1)
        ->orderBy('name')
        ->get();
    }
}