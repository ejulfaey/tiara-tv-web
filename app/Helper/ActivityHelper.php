<?php

/**
 * Add activity
 *
 * * @param code
 *
 * @return collection
 */
if (! function_exists('addActivity')) {
    function addActivity($action, $desc)
    {
        if($action == "create") {
            $extra = " has been added";
        } else if($action == "update"){
            $extra = " has been updated";
        } else {
            $extra = " has been deleted";
        }
        $activity = new App\Models\Activity;
        $activity->user_id = auth()->user()->id;
        $activity->action = $action;
        $activity->description = $desc . $extra;
        $activity->save();
    }
}