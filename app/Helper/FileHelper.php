<?php

/**
 * Store File
 *
 * * @param file
 *
 * @return path
 */
if (! function_exists('storeFile')) {
    function storeFile($path, $name, $file)
    {
        $name = $name . "." . $file->extension();
        $file->move($path, $name);
        return '/' . $path . $name;
    }
}

/**
 * Create Directory
 *
 * * @param none
 *
 * @return path
 */
if (! function_exists('createDirectory')) {
    function createDirectory($storage_path, $code)
    {
        $folder = date('Y-m-d');
        $storage = $storage_path . "/" . $folder;
        if(!file_exists($storage)){
            mkdir($storage);
        }
        $path = $storage . "/" . $code;
        if(!file_exists($path)) mkdir($path);
        return $path . "/";

    }
}

/**
 * Get storage path
 *
 * * @param none
 *
 * @return path
 */
if (! function_exists('getStoragePath')) {
    function getStoragePath($path)
    {
        $paths = explode("/", $path);
        $link = "";

        $found = 0;
        for($i = 0; $i < count($paths); $i++){

            if($paths[$i] == "storage" || $found > 0){
                if($found == 0) $link .= $paths[$i];
                else $link .= "/" . $paths[$i];
                $found++;
            }
        }

        return $link;
    }
}

/**
 * Delete file
 *
 * * @param path
 *
 * @return none
 */
if (! function_exists('deleteFile')) {
    function deleteFile($path)
    {
        if(File::exists($path)) {
            unlink($path);
        }
    }
}