<?php

/**
 * Get default date
 *
 * * @param none
 *
 * @return "08 Jun 2019"
 */
if (! function_exists('getDefaultDate')) {
    function getDefaultDate($date)
    {
        return date('d M Y', strtotime($date));
    }
}

/**
 * Get format human readable
 *
 * * @param none
 *
 * @return "2 days ago"
 */
if (! function_exists('getHumanDate')) {
    function getHumanDate($date)
    {
        return Carbon\Carbon::parse($date)->diffForHumans();
    }
}

/**
 * Get format human readable
 *
 * * @param none
 *
 * @return "2 days ago"
 */
if (! function_exists('getDefaultTime')) {
    function getDefaultTime($date)
    {
        return date('H:i A', strtotime($date));
    }
}