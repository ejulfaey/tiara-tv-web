<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\VideoRequest;
use App\Models\Video;


class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::whereDeleted(0)
        ->latest()
        ->get();

        return view('dashboard.pages.video.index', compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.pages.video.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoRequest $request)
    {

        $code = str_random(6);

        $video = new Video;
        $video->code = $code;
        $video->name = $request->name;
        $video->link = $request->link;
        $video->category_id = $request->category_id;
        $video->subcategory_id = $request->subcategory_id;
        $video->desc = $request->desc;
        $video->save();

        $path = createDirectory("storage/videos", $code);


        addActivity("create", "Video: " . $video->name);
        session()->flash('success', "Video has been created");
        return redirect()->route('video.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::find($id);
        return view('dashboard.pages.video.update', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $video = Video::find($id);
        $video->name = $request->name;
        $video->link = $request->link;
        $video->category_id = $request->category_id;
        $video->subcategory_id = $request->subcategory_id;
        $video->desc = $request->desc;
        $video->save();

        addActivity("update", "Video: " . $video->name);
        session()->flash('success', "Video has been updated");
        return redirect()->route('video.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::find($id);

        $video->deleted = true;
        $video->update();

        addActivity("delete", "Video: " . $video->name);
        return response()->json([
            'status' => 200
        ]);

    }
}
