<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\ProductImage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::whereDeleted(0)
        ->latest()
        ->get();

        return view('dashboard.pages.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.pages.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {

        $code = str_random(6);

        $product = new Product;
        $product->code = $code;
        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->category_id = $request->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->price = $request->price;
        $product->desc = $request->desc;
        $product->tags = $request->tags;
        $product->status = $request->status;
        $product->save();

        $path = createDirectory("storage/products", $code);

        foreach($request->image as $index => $image) {
            $product_image = new ProductImage;
            $product_image->product_id = $product->id;
            $product_image->image = storeFile($path, str_random(3), $request->file('image')[$index]);
            $product_image->save();
        }

        addActivity("create", "Product: " . $product->name);
        session()->flash('success', "Product has been created");
        return redirect()->route('product.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('dashboard.pages.product.update', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->category_id = $request->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->price = $request->price;
        $product->desc = $request->desc;
        $product->tags = $request->tags;
        $product->status = $request->status;
        $product->save();

        if($request->hasFile('image')) {
            $path = createDirectory("storage/products", $product->code);

            foreach($request->image as $index => $image) {
                $product_image = new ProductImage;
                $product_image->product_id = $product->id;
                $product_image->image = storeFile($path, str_random(3), $request->file('image')[$index]);
                $product_image->save();
            }
        }

        addActivity("update", "Product: " . $product->name);
        session()->flash('success', "Product has been updated");
        return redirect()->route('product.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        foreach($product->images as $image){
            $path = getStoragePath($image->image);
            deleteFile($path);
            $image->delete();
        }

        $product->status = false;
        $product->deleted = true;
        $product->update();

        addActivity("delete", "Product: " . $product->name);
        return response()->json([
            'status' => 200
        ]);

    }
}
