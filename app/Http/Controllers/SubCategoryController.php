<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SubCategoryRequest;
use App\Models\SubCategory;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = SubCategory::whereDeleted(0)
        ->latest()
        ->get();

        return view('dashboard.pages.subcategory.index', compact('subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.pages.subcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubCategoryRequest $request)
    {
        $subcategory = new SubCategory;
        $subcategory->category_id = $request->category_id;
        $subcategory->name = $request->name;
        $subcategory->desc = $request->desc;
        $subcategory->status = $request->status;

        if($request->hasFile('image')) {
            $code = str_random(6);
            $path = createDirectory("storage/subcategories", $code);
            $subcategory->image = storeFile($path, $code, $request->file('image'));
        } else {
            $subcategory->image = "https://image.flaticon.com/icons/svg/1141/1141913.svg";
        }

        $subcategory->save();

        addActivity("create", "SubCategory: " . $subcategory->name);
        session()->flash('success', "SubCategory has been created");
        return redirect()->route('subcategory.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategory = SubCategory::find($id);
        return view('dashboard.pages.subcategory.update', compact('subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubCategoryRequest $request, $id)
    {
        $subcategory = SubCategory::find($id);
        $subcategory->category_id = $request->category_id;
        $subcategory->name = $request->name;
        $subcategory->desc = $request->desc;
        $subcategory->status = $request->status;

        if($request->hasFile('image')) {
            $code = str_random(6);
            $path = createDirectory("storage/subcategories", $code);
            $subcategory->image = storeFile($path, $code, $request->file('image'));
        }
        $subcategory->update();

        addActivity("update", "SubCategory: " . $subcategory->name);
        session()->flash('success', "SubCategory has been updated");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = SubCategory::find($id);
        if($subcategory->image) {
            $path = getStoragePath($subcategory->image);
            deleteFile($path);
        }
        $subcategory->status = false;
        $subcategory->deleted = true;
        $subcategory->update();

        addActivity("delete", "subCategory: " . $subcategory->name);
        return response()->json([
            'status' => 200
        ]);
    }
}
