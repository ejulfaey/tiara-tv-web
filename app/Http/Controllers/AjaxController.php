<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\Models\ProductImage;

class AjaxController extends Controller
{
    public function delete_photo($id)
    {
        $image = ProductImage::find($id);
        $images = ProductImage::whereProductId($image->product_id)
        ->get();

        $status = 500;

        if($image){

            if(count($images) > 1) {
                $status = 200;
                $path = getStoragePath($image->image);
                deleteFile($path);
                $image->delete();

                addActivity("delete", "Product: " . $image->product_id . ", image");
                
            } else {
                $status = 404;
            }
        }

        return response()->json([
            'status' => $status
        ]);
    }

    public function delete_slider($code)
    {
        $slider = Setting::whereCode($code)
        ->first();

        $status = 500;

        if($slider) {
            $status = 200;
            $path = getStoragePath($slider->value);
            deleteFile($path);
            $slider->value = "";
            $slider->update();

            addActivity("delete", "Slider: " . $code . ", slider");
            
        } else {
            $status = 404;
        }
        return response()->json([
            'status' => $status
        ]);

    }

}
