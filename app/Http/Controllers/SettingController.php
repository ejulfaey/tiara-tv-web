<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::all();

        return view('dashboard.setting', compact('settings'));
    }

    public function basic(Request $request)
    {
        Setting::whereCode('title')
        ->update([
            'value' => $request->title
        ]);
        Setting::whereCode('description')
        ->update([
            'value' => $request->description
        ]);
        Setting::whereCode('email')
        ->update([
            'value' => $request->email
        ]);
        Setting::whereCode('contact_no')
        ->update([
            'value' => $request->contact_no
        ]);
        Setting::whereCode('address')
        ->update([
            'value' => $request->address
        ]);

        addActivity("update", "Setting, General setting");
        session()->flash('success', 'General setting has been updated');
        return back();

    }

    public function social(Request $request)
    {
        Setting::whereCode('facebook')
        ->update([
            'value' => $request->facebook
        ]);
        Setting::whereCode('twitter')
        ->update([
            'value' => $request->twitter
        ]);
        Setting::whereCode('instagram')
        ->update([
            'value' => $request->instagram
        ]);

        addActivity("update", "Social Account, Social setting");
        session()->flash('success', 'Social setting has been updated');
        return back();

    }

    public function payment(Request $request)
    {
        Setting::whereCode('currency')
        ->update([
            'value' => $request->currency
        ]);
        Setting::whereCode('min_donation')
        ->update([
            'value' => $request->min_donation
        ]);
        Setting::whereCode('sandbox')
        ->update([
            'value' => $request->sandbox
        ]);
        Setting::whereCode('billplz_secret_key')
        ->update([
            'value' => $request->billplz_secret_key
        ]);

        addActivity("update", "Payment Gateway, Payment Gateway Setting");
        session()->flash('success', 'Payment setting has been updated');
        return back();

    }

    public function icon(Request $request)
    {
        if($request->hasFile('main_logo')){

            $path = getStoragePath(getSetting('main_logo'));
            deleteFile($path);
            $code = "MAIN_LOGO";

            $path = createDirectory("storage/logos", $code);
            $main_logo = storeFile($path, $code, $request->file('main_logo'));

            Setting::whereCode('main_logo')
            ->update([
                'value' => $main_logo
            ]);
    
        }

        if($request->hasFile('small_logo')){

            $path = getStoragePath(getSetting('small_logo'));
            deleteFile($path);
            $code = "SMALL_LOGO";

            $path = createDirectory("storage/logos", $code);
            $small_logo = storeFile($path, $code, $request->file('small_logo'));

            Setting::whereCode('small_logo')
            ->update([
                'value' => $small_logo
            ]);
    
        }

        addActivity("update", "Icon, Icon setting");
        session()->flash('success', 'Icon has been updated');
        return back();
    }

    public function slider(Request $request)
    {
        if($request->hasFile('section_bg')){

            $path = getStoragePath(getSetting('section_bg'));
            deleteFile($path);
            $code = "section_bg";

            $path = createDirectory("storage/sliders", $code);
            $section_bg = storeFile($path, $code, $request->file('section_bg'));

            Setting::whereCode('section_bg')
            ->update([
                'value' => $section_bg
            ]);
    
        }

        if($request->hasFile('slider_1')){

            $path = getStoragePath(getSetting('slider_1'));
            deleteFile($path);
            $code = "slider_1";

            $path = createDirectory("storage/sliders", $code);
            $slider_1 = storeFile($path, $code, $request->file('slider_1'));

            Setting::whereCode('slider_1')
            ->update([
                'value' => $slider_1
            ]);
    
        }

        if($request->hasFile('slider_2')){

            $path = getStoragePath(getSetting('slider_2'));
            deleteFile($path);
            $code = "slider_2";

            $path = createDirectory("storage/sliders", $code);
            $slider_2 = storeFile($path, $code, $request->file('slider_2'));

            Setting::whereCode('slider_2')
            ->update([
                'value' => $slider_2
            ]);
    
        }

        if($request->hasFile('slider_3')){

            $path = getStoragePath(getSetting('slider_3'));
            deleteFile($path);
            $code = "slider_3";

            $path = createDirectory("storage/sliders", $code);
            $slider_3 = storeFile($path, $code, $request->file('slider_3'));

            Setting::whereCode('slider_3')
            ->update([
                'value' => $slider_3
            ]);
    
        }
        
        if($request->hasFile('slider_4')){

            $path = getStoragePath(getSetting('slider_4'));
            deleteFile($path);
            $code = "slider_4";

            $path = createDirectory("storage/sliders", $code);
            $slider_4 = storeFile($path, $code, $request->file('slider_4'));

            Setting::whereCode('slider_4')
            ->update([
                'value' => $slider_4
            ]);
    
        }

        if($request->hasFile('slider_5')){

            $path = getStoragePath(getSetting('slider_5'));
            deleteFile($path);
            $code = "slider_5";

            $path = createDirectory("storage/sliders", $code);
            $slider_5 = storeFile($path, $code, $request->file('slider_5'));

            Setting::whereCode('slider_5')
            ->update([
                'value' => $slider_5
            ]);
    
        }

        addActivity("update", "Banner, Slider setting");
        session()->flash('success', 'Banner slider has been updated');
        return back();
    }

}