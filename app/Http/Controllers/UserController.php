<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\User;

use App\Services\ImageService;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereDeleted(0)
        ->latest()
        ->get();

        return view('dashboard.pages.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.pages.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if(auth()->user()->role != "admin") {
            return back();
        }
        $user = new User;
        $user->fullname = $request->fullname;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->role = $request->role;

        $code = str_random(7);
        $user->code = $code;

        // Create Avatar
        if($request->hasFile('image')){
            $path = createDirectory("storage/users", $code);
            $user->image = storeFile($path, $code, $request->file('image'));
        } else {
            $user->image = "https://image.flaticon.com/icons/svg/149/149071.svg";
        }

        $user->save();

        addActivity("create", "User: " . $user->fullname);
        session()->flash('success', "User: " . $user->fullname . " is created");
        return redirect()->route('user.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('dashboard.pages.user.update', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::find($id);
        $user->fullname = $request->fullname;

        if(auth()->user()->role == "admin") {
            $user->role = $request->role;
        }

        if($request->filled('password')){
            $user->password = bcrypt($request->password);
        }

        // Update Avatar
        if($request->hasFile('image')){

            $path = getStoragePath($user->image);
            deleteFile($path);

            $path = createDirectory("storage/users", $user->code);
            $user->image = storeFile($path, $user->code, $request->file('image'));
        }

        $user->update();

        addActivity("update", "User: " . $user->fullname);
        session()->flash('success', "User: " . $user->fullname . " has been updated");
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $path = getStoragePath($user->image);
        deleteFile($path);
        $user->status = false;
        $user->deleted = true;
        $user->update();

        addActivity("delete", "User: " . $user->fullname);
        return response()->json([
            'status' => 200
        ]);
    }
}
