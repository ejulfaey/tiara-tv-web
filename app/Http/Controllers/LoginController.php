<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;

use App\User;
use App\Models\Login;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function login(LoginRequest $request)
    {
        $auth = auth()->attempt([
            'username' => $request->username,
            'password' => $request->password
        ]);

        if($auth){
            
            $login = new Login;
            $login->user_id = auth()->user()->id;
            $login->ip_address = $request->ip();
            $login->save();

            $user = User::find(auth()->user()->id);
            $user->login_at = now();
            $user->update();

            session()->flash('success', 'Welcome ' . auth()->user()->fullname);
            return redirect()->route('dashboard');

        } else {
            session()->flash('error', 'Your password might be wrong!');
            return back();
        }
    }

    public function logout()
    {
        session()->flush();
        auth()->logout();

        return redirect()->route('logout');
    }
}