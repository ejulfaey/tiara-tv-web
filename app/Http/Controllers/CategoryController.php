<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::whereDeleted(0)
        ->latest()
        ->get();

        return view('dashboard.pages.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.pages.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $category = new Category;
        $category->name = $request->name;
        $category->desc = $request->desc;
        $category->status = $request->status;

        if($request->hasFile('image')) {
            $code = str_random(6);
            $path = createDirectory("storage/categories", $code);
            $category->image = storeFile($path, $code, $request->file('image'));
        } else {
            $category->image = "https://image.flaticon.com/icons/svg/1141/1141913.svg";
        }

        $category->save();

        addActivity("create", "Category: " . $category->name);
        session()->flash('success', "Category has been created");
        return redirect()->route('category.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('dashboard.pages.category.update', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->name;
        $category->desc = $request->desc;
        $category->status = $request->status;

        if($request->hasFile('image')) {
            $code = str_random(6);
            $path = createDirectory("storage/categories", $code);
            $category->image = storeFile($path, $code, $request->file('image'));
        }
        $category->update();

        addActivity("update", "Category: " . $category->name);
        session()->flash('success', "Category has been updated");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if($category->image) {
            $path = getStoragePath($category->image);
            deleteFile($path);
        }
        $category->status = false;
        $category->deleted = true;
        $category->update();

        addActivity("delete", "Category: " . $category->name);
        return response()->json([
            'status' => 200
        ]);
    }
}
