<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateInspectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'component_id' => 'required',
            'defect_id' => 'required',
            'condition_rating' => 'required',
            'maintenance_rating' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'component_id.required' => 'Component is required',
            'defect_id.required' => 'Defect is required',
            'condition_rating.required' => 'Condition Rating is required',
            'maintenance_rating.required' => 'Maintenance Rating is required',
        ];
    }
}
