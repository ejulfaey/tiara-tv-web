<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required',
            'password' => 'confirmed',
            'password_confirmation' => 'required_with:password',
            'role' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'fullname.required' => 'Full Name is required',
            'password.confirmed' => 'Both passwords not match',
            'password_confirmation.required_with' => 'Re-type your password',
            'role.required' => 'Role is required',
        ];
    }
}
