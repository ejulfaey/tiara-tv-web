<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required',
            'username' => 'required|unique:users|regex:/^\S*$/u',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
            'role' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'fullname.required' => 'Full Name is required',
            'username.required' => 'User Name is required',
            'username.regex' => 'User Name should be one word',
            'username.unique' => 'User Name has been taken',
            'password.required' => 'Password is required',
            'password.min' => 'Password should be at least 6 characters',
            'password.confirmed' => 'Both passwords not match',
            'password_confirmation.required' => 'Re-type your password',
            'role.required' => 'Role is required',
        ];
    }
}
