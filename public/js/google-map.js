function Location(lat, lng){
    this.lat = lat;
    this.lng = lng;
    this.getLat = function(){
        return this.lat;
    }
    this.getLng = function(){
        return this.lng;
    }
    this.getLatLng = function(){
        return {
            lat: this.lat,
            lng: this.lng
        }
    }
    $("#lat").val(this.lat);
    $("#lng").val(this.lng);
}
function showPosition(position) {
    pos = new Location(position.coords.latitude, position.coords.longitude);
    initMap();
    get3words();
    reverseGeocode();
}
function initialize() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
    var input = document.getElementById('address');
    var autocomplete = new google.maps.places.Autocomplete(input);
}
google.maps.event.addDomListener(window, 'load', initialize);
geocoder = new google.maps.Geocoder();

function initMap(){
    var myCenter = new google.maps.LatLng(pos.getLat(), pos.getLng());
    var mapCanvas = document.getElementById("google_map");
    var mapOptions = {
        center: myCenter,
        zoom: 16,
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var marker = new google.maps.Marker({
        position: myCenter,
        draggable: true
    });
    marker.addListener('dragend', function(data){
        pos = new Location(marker.position.lat(), marker.position.lng());
        console.log(pos);
        get3words();
        reverseGeocode();
    });
    marker.setMap(map);
}
function getPosition(address){
    geocoder.geocode( { 'address' : address }, function( res, status ) {
        console.log(res);
        var addresses = res[0].address_components;
        pos = new Location(res[0].geometry.location.lat(), res[0].geometry.location.lng());
        initMap();
        get3words();
    });
}
function get3words(){
    let url = 'https://api.what3words.com/v2/reverse?coords='+ pos.getLat() +','+ pos.getLng() +'&display=full&format=json&key=FF5NJWGH';
    $.get(url, function(res){
        $("#w3w").val(res.words);
    });
}
function reverseGeocode(){
    geocoder.geocode({'location': pos.getLatLng()}, function(res) {
        $("#address").val(res[0].formatted_address);
        console.log(res);
    });
}
$('#address').change(function(){
    var address;
    setTimeout(function(){
        address = $('#address').val();
        getPosition(address);
    }, 100);
});