$('a.btn-danger').on('click', function(event){
    event.preventDefault();
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        console.log(result);
        if(result.value) deleteItem($(this).data('func'), $(this).data('id'));
    })
});

function deleteItem(func, id){
    $.ajax({
        url: '/' + func + '/' + id,
        type: 'DELETE',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(res) {
            console.log(res);
            if(res.status == 200){
                swal({
                    title: 'Deleted',
                    type: 'success',
                    showCancelButton: true,
                    allowOutsideClick: () => !swal.isLoading()
                }).then((result) => {
                    location.reload();
                });
            } else {
                swal({
                    title: "Can not proceed",
                    text: res.msg,
                    type: 'error',
                    allowOutsideClick: () => !swal.isLoading()
                });
            }
        }
    });
}