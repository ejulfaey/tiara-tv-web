<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'fullname' => 'Admin John',
                'username' => 'admin',
                'password' => bcrypt('123qwe'),
                'code' => str_random(7),
                'image' => "https://image.flaticon.com/icons/svg/149/149071.svg",
                'role' => 'admin',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'fullname' => 'Lewandoski',
                'username' => 'user1',
                'password' => bcrypt('123qwe'),
                'code' => str_random(7),
                'image' => "https://image.flaticon.com/icons/svg/149/149071.svg",
                'role' => 'user',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
