<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'code' => 'title',
                'value' => 'E-Commerce'
            ],
            [
                'code' => 'description',
                'value' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum officia adipisci nisi, saepe enim iusto sequi esse perspiciatis optio id nulla exercitationem in rerum, excepturi neque similique distinctio laudantium dolor.'
            ],
            [
                'code' => 'email',
                'value' => 'support@ecommerce.com'
            ],
            [
                'code' => 'contact_no',
                'value' => '+607-7788999'
            ],
            [
                'code' => 'address',
                'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit'
            ],
            [
                'code' => 'facebook',
                'value' => 'https://facebook.com/'
            ],
            [
                'code' => 'twitter',
                'value' => 'https://twitter.com/'
            ],
            [
                'code' => 'instagram',
                'value' => 'https://instagram.com/'
            ],
            [
                'code' => 'currency',
                'value' => 'RM'
            ],
            [
                'code' => 'billplz_secret_key',
                'value' => 'e0def5e4-5c7d-4ac8-93d5-03473b256eb5'
            ],
            [
                'code' => 'sandbox',
                'value' => 'yes'
            ],
            [
                'code' => 'section_bg',
                'value' => 'https://cdn.pixabay.com/photo/2016/03/16/16/27/puzzle-1261138_1280.jpg',
            ],
            [
                'code' => 'slider_1',
                'value' => 'http://markup.htmlmate.com/fundme/rev-slider/img/slider-bg.jpg',
            ],
            [
                'code' => 'slider_2',
                'value' => 'http://markup.htmlmate.com/fundme/rev-slider/img/slider-bg.jpg',
            ],
            [
                'code' => 'slider_3',
                'value' => 'http://markup.htmlmate.com/fundme/rev-slider/img/slider-bg.jpg',
            ],
            [
                'code' => 'slider_4',
                'value' => 'http://markup.htmlmate.com/fundme/rev-slider/img/slider-bg.jpg',
            ],
            [
                'code' => 'slider_5',
                'value' => 'http://markup.htmlmate.com/fundme/rev-slider/img/slider-bg.jpg',
            ],
            [
                'code' => 'main_logo',
                'value' => 'http://markup.htmlmate.com/fundme/assets/img/logo.png',
            ],
            [
                'code' => 'small_logo',
                'value' => 'http://markup.htmlmate.com/fundme/assets/img/icon.png',
            ],

        ]);
    }
}
