<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'id' => 1,
                'code' => 'admin',
                'name' => 'Admin',                
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 2,
                'code' => 'user',
                'name' => 'User',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
