<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->integer('user_id')->nullable();
            $table->string('fullname')->nullable();
            $table->string('email')->nullable();
            $table->string('phone_no')->nullable();
            $table->text('address')->nullable();
            $table->text('remarks')->nullable();
            $table->decimal('discount', 7, 2)->default(0.0);
            $table->decimal('tax', 7, 2)->default(0.0);
            $table->decimal('shipping_fee', 7, 2)->default(0.0);
            $table->decimal('total_price', 7, 2)->default(0.0);
            $table->string('tracking_no')->nullable();
            $table->string('payment_status')->default('pending')->comment('pending', 'success', 'failed');
            $table->string('order_status')->default('ready to deliver')->comment('ready to deliver', 'shipping', 'delivered', 'returned');
            $table->integer('pic')->nullable();
            $table->string('bill_id')->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
