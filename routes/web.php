<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('guest')->group(function(){

    Route::name('index')->get('/', 'LoginController@index');
    Route::name('login')->get('/', 'LoginController@index');
    Route::name('login.post')->post('/login/post', 'LoginController@login');

});

Route::middleware('auth')->group(function(){

    Route::middleware('admin')->group(function(){

        Route::name('dashboard')->get('dashboard', 'DashboardController@index');
        Route::name('home')->get('home', 'DashboardController@index');
        Route::name('activity')->get('activity', 'ActivityController@index');
        Route::name('logout')->get('logout', 'LoginController@logout');
    
        Route::resources([
            'product' => 'ProductController',
            'video' => 'VideoController', //add
            'order' => 'OrderController',
            'category' => 'CategoryController',
            'subcategory' => 'SubCategoryController',
            'user' => 'UserController',
            'role' => 'RoleController',
            'login_history' => 'LoginHistoryController',
        ]);
    
        Route::name('setting.')->prefix('setting')->group(function(){
    
            Route::name('index')->get('/','SettingController@index');
            Route::name('basic')->post('basic','SettingController@basic');
            Route::name('social')->post('social','SettingController@social');
            Route::name('payment')->post('payment','SettingController@payment');
            Route::name('icon')->post('icon','SettingController@icon');
            Route::name('slider')->post('slider','SettingController@slider');
    
        });
    
        Route::name('ajax.')->prefix('ajax')->group(function(){
            
            Route::name('delete_slider')->get('delete_slider/{id}', 'AjaxController@delete_slider');    
            Route::name('delete_photo')->get('delete_photo/{id}', 'AjaxController@delete_photo');    
            
        });
    
    });

});